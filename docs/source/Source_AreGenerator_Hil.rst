Hil
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:HIL:RATE
	single: [SOURce<HW>]:AREGenerator:HIL:RECeived

.. code-block:: python

	[SOURce<HW>]:AREGenerator:HIL:RATE
	[SOURce<HW>]:AREGenerator:HIL:RECeived



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Hil.HilCls
	:members:
	:undoc-members:
	:noindex: