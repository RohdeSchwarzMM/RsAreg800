Connect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CONNect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CONNect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Connect.ConnectCls
	:members:
	:undoc-members:
	:noindex: