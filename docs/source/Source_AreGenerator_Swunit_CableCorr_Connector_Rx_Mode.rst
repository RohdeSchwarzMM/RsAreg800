Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:RX<ST>:MODE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:RX<ST>:MODE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.CableCorr.Connector.Rx.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: