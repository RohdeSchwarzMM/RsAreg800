Snumber
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SNUMber

.. code-block:: python

	SENSe<CH>:[POWer]:SNUMber



.. autoclass:: RsAreg800.Implementations.Sense.Power.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: