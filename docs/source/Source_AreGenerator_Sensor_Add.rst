Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:ADD

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:ADD



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Add.AddCls
	:members:
	:undoc-members:
	:noindex: