Service
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic<HW>:SERVice:SFUNction
	single: DIAGnostic:SERVice

.. code-block:: python

	DIAGnostic<HW>:SERVice:SFUNction
	DIAGnostic:SERVice



.. autoclass:: RsAreg800.Implementations.Diagnostic.Service.ServiceCls
	:members:
	:undoc-members:
	:noindex: