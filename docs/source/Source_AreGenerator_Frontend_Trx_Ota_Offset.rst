Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:OTA:OFFSet

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:OTA:OFFSet



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Ota.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: