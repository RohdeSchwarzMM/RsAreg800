Year
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: HCOPy:FILE:[NAME]:AUTO:[FILE]:YEAR:STATe

.. code-block:: python

	HCOPy:FILE:[NAME]:AUTO:[FILE]:YEAR:STATe



.. autoclass:: RsAreg800.Implementations.HardCopy.File.Name.Auto.File.Year.YearCls
	:members:
	:undoc-members:
	:noindex: