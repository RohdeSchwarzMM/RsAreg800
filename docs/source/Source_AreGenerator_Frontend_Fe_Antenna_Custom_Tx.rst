Tx<TxIndexNull>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr0 .. Nr15
	rc = driver.source.areGenerator.frontend.fe.antenna.custom.tx.repcap_txIndexNull_get()
	driver.source.areGenerator.frontend.fe.antenna.custom.tx.repcap_txIndexNull_set(repcap.TxIndexNull.Nr0)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Antenna.Custom.Tx.TxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.antenna.custom.tx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Tx_Glist.rst