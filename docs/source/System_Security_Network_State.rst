State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:[STATe]



.. autoclass:: RsAreg800.Implementations.System.Security.Network.State.StateCls
	:members:
	:undoc-members:
	:noindex: