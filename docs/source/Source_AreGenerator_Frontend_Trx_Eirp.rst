Eirp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:EIRP

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:EIRP



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Eirp.EirpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.trx.eirp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Trx_Eirp_Port.rst
	Source_AreGenerator_Frontend_Trx_Eirp_Sensor.rst