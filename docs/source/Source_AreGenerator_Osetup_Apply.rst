Apply
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:APPLy

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:APPLy



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: