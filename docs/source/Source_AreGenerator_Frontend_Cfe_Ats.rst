Ats
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:ATS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:ATS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Ats.AtsCls
	:members:
	:undoc-members:
	:noindex: