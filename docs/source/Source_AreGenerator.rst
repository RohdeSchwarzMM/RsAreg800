AreGenerator
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.AreGeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Channel.rst
	Source_AreGenerator_Dlogging.rst
	Source_AreGenerator_Frontend.rst
	Source_AreGenerator_Hil.rst
	Source_AreGenerator_Last.rst
	Source_AreGenerator_Mapping.rst
	Source_AreGenerator_Marker.rst
	Source_AreGenerator_Measurement.rst
	Source_AreGenerator_Object.rst
	Source_AreGenerator_Objects.rst
	Source_AreGenerator_Omonitoring.rst
	Source_AreGenerator_Osetup.rst
	Source_AreGenerator_Radar.rst
	Source_AreGenerator_Scenario.rst
	Source_AreGenerator_Sensor.rst
	Source_AreGenerator_Swunit.rst
	Source_AreGenerator_Units.rst