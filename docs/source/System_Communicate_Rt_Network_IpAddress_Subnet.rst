Subnet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:RT:NETWork:[IPADdress]:SUBNet:MASK

.. code-block:: python

	SYSTem:COMMunicate:RT:NETWork:[IPADdress]:SUBNet:MASK



.. autoclass:: RsAreg800.Implementations.System.Communicate.Rt.Network.IpAddress.Subnet.SubnetCls
	:members:
	:undoc-members:
	:noindex: