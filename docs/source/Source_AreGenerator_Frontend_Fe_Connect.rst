Connect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CONNect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CONNect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Connect.ConnectCls
	:members:
	:undoc-members:
	:noindex: