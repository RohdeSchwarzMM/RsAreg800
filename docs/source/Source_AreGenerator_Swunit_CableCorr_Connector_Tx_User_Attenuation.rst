Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:TX<ST0>:USER:ATTenuation

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:TX<ST0>:USER:ATTenuation



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.CableCorr.Connector.Tx.User.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: