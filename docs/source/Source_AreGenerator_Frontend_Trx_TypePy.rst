TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:TYPE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:TYPE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: