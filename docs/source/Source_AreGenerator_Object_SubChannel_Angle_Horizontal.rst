Horizontal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:ANGLe:HORizontal

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:ANGLe:HORizontal



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Object.SubChannel.Angle.Horizontal.HorizontalCls
	:members:
	:undoc-members:
	:noindex: