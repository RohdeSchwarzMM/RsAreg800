Otime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MAPPing<CH>:ADJust:LEVel:OTIMe

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MAPPing<CH>:ADJust:LEVel:OTIMe



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.Adjust.Level.Otime.OtimeCls
	:members:
	:undoc-members:
	:noindex: