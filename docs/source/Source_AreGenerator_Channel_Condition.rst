Condition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:CONDition:INFO
	single: [SOURce<HW>]:AREGenerator:CHANnel:CONDition

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:CONDition:INFO
	[SOURce<HW>]:AREGenerator:CHANnel:CONDition



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Channel.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: