Alias
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:ALIas

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:ALIas



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Alias.AliasCls
	:members:
	:undoc-members:
	:noindex: