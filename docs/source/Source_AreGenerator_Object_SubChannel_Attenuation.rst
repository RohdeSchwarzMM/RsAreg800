Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:ATTenuation

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:ATTenuation



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Object.SubChannel.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: