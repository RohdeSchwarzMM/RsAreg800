Snumber
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:SNUMber

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:SNUMber



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: