Ota
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Rx.Ota.OtaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.cfe.rx.ota.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Cfe_Rx_Ota_Offset.rst