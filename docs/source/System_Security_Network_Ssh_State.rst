State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:SSH:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:SSH:[STATe]



.. autoclass:: RsAreg800.Implementations.System.Security.Network.Ssh.State.StateCls
	:members:
	:undoc-members:
	:noindex: