Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:[LEVel]:[IMMediate]:RCL

.. code-block:: python

	[SOURce<HW>]:POWer:[LEVel]:[IMMediate]:RCL



.. autoclass:: RsAreg800.Implementations.Source.Power.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: