Efrontend
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:TX<ST0>:EFRontend

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:TX<ST0>:EFRontend



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Tx.Efrontend.EfrontendCls
	:members:
	:undoc-members:
	:noindex: