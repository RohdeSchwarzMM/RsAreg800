Sensor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:EIRP:SENSor

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:EIRP:SENSor



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Eirp.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex: