Store
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:ROSCillator:STORe

.. code-block:: python

	CALibration:ROSCillator:STORe



.. autoclass:: RsAreg800.Implementations.Calibration.Roscillator.Store.StoreCls
	:members:
	:undoc-members:
	:noindex: