State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:LOGGing:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:LOGGing:STATe



.. autoclass:: RsAreg800.Implementations.Sense.Power.Logging.State.StateCls
	:members:
	:undoc-members:
	:noindex: