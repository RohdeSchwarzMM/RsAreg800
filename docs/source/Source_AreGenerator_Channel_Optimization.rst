Optimization
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:OPTimization:MODE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:OPTimization:MODE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Channel.Optimization.OptimizationCls
	:members:
	:undoc-members:
	:noindex: