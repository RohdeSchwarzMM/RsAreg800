Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:LEVel:MEASured

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:LEVel:MEASured



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Channel.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: