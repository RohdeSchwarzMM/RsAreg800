Valid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJects:VALid:CATalog
	single: [SOURce<HW>]:AREGenerator:OBJects:VALid

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJects:VALid:CATalog
	[SOURce<HW>]:AREGenerator:OBJects:VALid



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Objects.Valid.ValidCls
	:members:
	:undoc-members:
	:noindex: