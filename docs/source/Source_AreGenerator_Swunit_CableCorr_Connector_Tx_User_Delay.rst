Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:TX<ST0>:USER:DELay

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:TX<ST0>:USER:DELay



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.CableCorr.Connector.Tx.User.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: