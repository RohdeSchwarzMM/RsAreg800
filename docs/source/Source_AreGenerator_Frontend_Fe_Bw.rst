Bw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:BW

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:BW



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Bw.BwCls
	:members:
	:undoc-members:
	:noindex: