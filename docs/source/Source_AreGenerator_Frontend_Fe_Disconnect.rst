Disconnect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:DISConnect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:DISConnect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Disconnect.DisconnectCls
	:members:
	:undoc-members:
	:noindex: