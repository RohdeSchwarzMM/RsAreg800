Port
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:EIRP:PORT

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:EIRP:PORT



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Eirp.Port.PortCls
	:members:
	:undoc-members:
	:noindex: