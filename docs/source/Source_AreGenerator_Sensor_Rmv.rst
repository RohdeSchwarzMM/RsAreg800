Rmv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:RMV

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:RMV



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Rmv.RmvCls
	:members:
	:undoc-members:
	:noindex: