TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:TYPE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:TYPE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: