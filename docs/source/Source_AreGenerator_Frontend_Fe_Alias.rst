Alias
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ALIas

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ALIas



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Alias.AliasCls
	:members:
	:undoc-members:
	:noindex: