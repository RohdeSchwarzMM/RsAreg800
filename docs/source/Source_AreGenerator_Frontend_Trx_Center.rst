Center
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CENTer

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CENTer



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Center.CenterCls
	:members:
	:undoc-members:
	:noindex: