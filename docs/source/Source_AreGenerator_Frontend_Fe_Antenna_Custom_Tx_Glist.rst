Glist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:TX<ST0>:GLISt

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:TX<ST0>:GLISt



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Antenna.Custom.Tx.Glist.GlistCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.antenna.custom.tx.glist.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Tx_Glist_Row.rst