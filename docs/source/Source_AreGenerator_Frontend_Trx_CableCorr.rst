CableCorr
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.CableCorr.CableCorrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.trx.cableCorr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Trx_CableCorr_Connector.rst