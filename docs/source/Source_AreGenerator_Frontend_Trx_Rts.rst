Rts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:RTS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:RTS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Rts.RtsCls
	:members:
	:undoc-members:
	:noindex: