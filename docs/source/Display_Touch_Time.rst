Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay:TOUCh:TIME:CHARge

.. code-block:: python

	DISPlay:TOUCh:TIME:CHARge



.. autoclass:: RsAreg800.Implementations.Display.Touch.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: