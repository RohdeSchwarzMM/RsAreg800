SubChannel<Subchannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.source.areGenerator.mapping.subChannel.repcap_subchannel_get()
	driver.source.areGenerator.mapping.subChannel.repcap_subchannel_set(repcap.Subchannel.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.SubChannel.SubChannelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.mapping.subChannel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Mapping_SubChannel_Adjust.rst
	Source_AreGenerator_Mapping_SubChannel_Fe.rst