Psensor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MAPPing<CH>:PSENsor

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MAPPing<CH>:PSENsor



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.Psensor.PsensorCls
	:members:
	:undoc-members:
	:noindex: