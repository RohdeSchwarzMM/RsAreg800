Shared
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:LOCK:REQuest:SHARed

.. code-block:: python

	SYSTem:LOCK:REQuest:SHARed



.. autoclass:: RsAreg800.Implementations.System.Lock.Request.Shared.SharedCls
	:members:
	:undoc-members:
	:noindex: