Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CABLecorr:CONNector<DI>:RX<ST>:USER:DELay

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CABLecorr:CONNector<DI>:RX<ST>:USER:DELay



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.CableCorr.Connector.Rx.User.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: