File
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:CUSTom:RX<ST>:FILE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:CUSTom:RX<ST>:FILE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Antenna.Custom.Rx.File.FileCls
	:members:
	:undoc-members:
	:noindex: