Or
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:OR

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:OR



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Or.OrCls
	:members:
	:undoc-members:
	:noindex: