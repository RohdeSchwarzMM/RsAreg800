Button
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay:BUTTon:BRIGhtness

.. code-block:: python

	DISPlay:BUTTon:BRIGhtness



.. autoclass:: RsAreg800.Implementations.Display.Button.ButtonCls
	:members:
	:undoc-members:
	:noindex: