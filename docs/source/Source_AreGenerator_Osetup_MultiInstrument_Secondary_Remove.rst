Remove
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:REMove

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:REMove



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.Remove.RemoveCls
	:members:
	:undoc-members:
	:noindex: