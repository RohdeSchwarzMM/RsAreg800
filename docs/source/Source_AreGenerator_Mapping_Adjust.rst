Adjust
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.Adjust.AdjustCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.mapping.adjust.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Mapping_Adjust_All.rst
	Source_AreGenerator_Mapping_Adjust_Level.rst