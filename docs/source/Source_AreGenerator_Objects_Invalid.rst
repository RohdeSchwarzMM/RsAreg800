Invalid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJects:INValid:CATalog
	single: [SOURce<HW>]:AREGenerator:OBJects:INValid

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJects:INValid:CATalog
	[SOURce<HW>]:AREGenerator:OBJects:INValid



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Objects.Invalid.InvalidCls
	:members:
	:undoc-members:
	:noindex: