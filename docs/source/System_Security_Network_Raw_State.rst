State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:RAW:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:RAW:[STATe]



.. autoclass:: RsAreg800.Implementations.System.Security.Network.Raw.State.StateCls
	:members:
	:undoc-members:
	:noindex: