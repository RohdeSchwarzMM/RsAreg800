Bw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:BW

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:BW



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Bw.BwCls
	:members:
	:undoc-members:
	:noindex: