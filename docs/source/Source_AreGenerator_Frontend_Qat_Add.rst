Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ADD

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ADD



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Add.AddCls
	:members:
	:undoc-members:
	:noindex: