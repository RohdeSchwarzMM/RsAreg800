Objects
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Objects.ObjectsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.objects.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Objects_Invalid.rst
	Source_AreGenerator_Objects_Valid.rst