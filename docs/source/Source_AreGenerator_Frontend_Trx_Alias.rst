Alias
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ALIas

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ALIas



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Alias.AliasCls
	:members:
	:undoc-members:
	:noindex: