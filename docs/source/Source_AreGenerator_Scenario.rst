Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:SCENario:PROGress
	single: [SOURce<HW>]:AREGenerator:SCENario:RESet
	single: [SOURce<HW>]:AREGenerator:SCENario:STARt
	single: [SOURce<HW>]:AREGenerator:SCENario:STATus
	single: [SOURce<HW>]:AREGenerator:SCENario:STOP

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SCENario:PROGress
	[SOURce<HW>]:AREGenerator:SCENario:RESet
	[SOURce<HW>]:AREGenerator:SCENario:STARt
	[SOURce<HW>]:AREGenerator:SCENario:STATus
	[SOURce<HW>]:AREGenerator:SCENario:STOP



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Scenario_File.rst
	Source_AreGenerator_Scenario_Pause.rst
	Source_AreGenerator_Scenario_Position.rst
	Source_AreGenerator_Scenario_Replay.rst