Center
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CENTer

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CENTer



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Center.CenterCls
	:members:
	:undoc-members:
	:noindex: