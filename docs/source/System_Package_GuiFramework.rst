GuiFramework
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PACKage:GUIFramework:VERSion

.. code-block:: python

	SYSTem:PACKage:GUIFramework:VERSion



.. autoclass:: RsAreg800.Implementations.System.Package.GuiFramework.GuiFrameworkCls
	:members:
	:undoc-members:
	:noindex: