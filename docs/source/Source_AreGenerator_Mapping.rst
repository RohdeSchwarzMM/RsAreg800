Mapping<MappingChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.areGenerator.mapping.repcap_mappingChannel_get()
	driver.source.areGenerator.mapping.repcap_mappingChannel_set(repcap.MappingChannel.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.MappingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.mapping.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Mapping_Adjust.rst
	Source_AreGenerator_Mapping_Psensor.rst
	Source_AreGenerator_Mapping_Sensor.rst
	Source_AreGenerator_Mapping_SubChannel.rst