Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:COUNt

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:COUNt



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Count.CountCls
	:members:
	:undoc-members:
	:noindex: