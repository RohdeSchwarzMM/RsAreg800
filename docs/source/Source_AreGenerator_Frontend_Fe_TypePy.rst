TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:TYPE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:TYPE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: