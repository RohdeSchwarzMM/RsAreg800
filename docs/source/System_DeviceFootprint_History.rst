History
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:DFPRint:HISTory:COUNt
	single: SYSTem:DFPRint:HISTory:ENTRy

.. code-block:: python

	SYSTem:DFPRint:HISTory:COUNt
	SYSTem:DFPRint:HISTory:ENTRy



.. autoclass:: RsAreg800.Implementations.System.DeviceFootprint.History.HistoryCls
	:members:
	:undoc-members:
	:noindex: