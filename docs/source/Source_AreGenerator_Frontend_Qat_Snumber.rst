Snumber
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:SNUMber

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:SNUMber



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: