Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:INPut:TRIGger:SLOPe

.. code-block:: python

	[SOURce]:INPut:TRIGger:SLOPe



.. autoclass:: RsAreg800.Implementations.Source.InputPy.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: