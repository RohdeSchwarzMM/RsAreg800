Common
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:SYST:NETWork:[COMMon]:HOSTname
	single: SYSTem:COMMunicate:SYST:NETWork:[COMMon]:WORKgroup

.. code-block:: python

	SYSTem:COMMunicate:SYST:NETWork:[COMMon]:HOSTname
	SYSTem:COMMunicate:SYST:NETWork:[COMMon]:WORKgroup



.. autoclass:: RsAreg800.Implementations.System.Communicate.Syst.Network.Common.CommonCls
	:members:
	:undoc-members:
	:noindex: