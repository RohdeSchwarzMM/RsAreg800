Swunit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:SWUNit:[STATe]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:SWUNit:[STATe]



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.Swunit.SwunitCls
	:members:
	:undoc-members:
	:noindex: