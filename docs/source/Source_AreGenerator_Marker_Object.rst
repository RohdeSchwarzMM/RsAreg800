Object
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:MARKer:OBJect:DELay
	single: [SOURce<HW>]:AREGenerator:MARKer:OBJect:ONTime
	single: [SOURce<HW>]:AREGenerator:MARKer:OBJect:SOURce

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MARKer:OBJect:DELay
	[SOURce<HW>]:AREGenerator:MARKer:OBJect:ONTime
	[SOURce<HW>]:AREGenerator:MARKer:OBJect:SOURce



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Marker.Object.ObjectCls
	:members:
	:undoc-members:
	:noindex: