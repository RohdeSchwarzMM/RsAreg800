Efrontend
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:RX<ST>:EFRontend

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:RX<ST>:EFRontend



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Rx.Efrontend.EfrontendCls
	:members:
	:undoc-members:
	:noindex: