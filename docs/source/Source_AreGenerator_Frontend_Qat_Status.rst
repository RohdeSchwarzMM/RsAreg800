Status
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:STATus

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:STATus



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Status.StatusCls
	:members:
	:undoc-members:
	:noindex: