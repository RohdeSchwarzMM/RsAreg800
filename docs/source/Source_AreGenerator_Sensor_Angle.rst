Angle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:ANGLe

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:ANGLe



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Angle.AngleCls
	:members:
	:undoc-members:
	:noindex: