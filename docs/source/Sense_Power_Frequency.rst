Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:FREQuency

.. code-block:: python

	SENSe<CH>:[POWer]:FREQuency



.. autoclass:: RsAreg800.Implementations.Sense.Power.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: