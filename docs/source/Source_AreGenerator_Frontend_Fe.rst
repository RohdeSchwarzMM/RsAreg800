Fe<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.areGenerator.frontend.fe.repcap_channel_get()
	driver.source.areGenerator.frontend.fe.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.FeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Fe_Add.rst
	Source_AreGenerator_Frontend_Fe_Alias.rst
	Source_AreGenerator_Frontend_Fe_Antenna.rst
	Source_AreGenerator_Frontend_Fe_Ats.rst
	Source_AreGenerator_Frontend_Fe_Bw.rst
	Source_AreGenerator_Frontend_Fe_CableCorr.rst
	Source_AreGenerator_Frontend_Fe_Center.rst
	Source_AreGenerator_Frontend_Fe_Connect.rst
	Source_AreGenerator_Frontend_Fe_Disconnect.rst
	Source_AreGenerator_Frontend_Fe_Rmv.rst
	Source_AreGenerator_Frontend_Fe_Rts.rst
	Source_AreGenerator_Frontend_Fe_Rx.rst
	Source_AreGenerator_Frontend_Fe_Status.rst
	Source_AreGenerator_Frontend_Fe_Tx.rst
	Source_AreGenerator_Frontend_Fe_TypePy.rst