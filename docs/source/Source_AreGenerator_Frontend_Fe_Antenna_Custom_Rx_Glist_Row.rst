Row<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.areGenerator.frontend.fe.antenna.custom.rx.glist.row.repcap_index_get()
	driver.source.areGenerator.frontend.fe.antenna.custom.rx.glist.row.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:RX<ST>:GLISt:ROW<DI>

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:RX<ST>:GLISt:ROW<DI>



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Antenna.Custom.Rx.Glist.Row.RowCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.antenna.custom.rx.glist.row.clone()