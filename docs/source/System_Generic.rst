Generic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:GENeric:MSG

.. code-block:: python

	SYSTem:GENeric:MSG



.. autoclass:: RsAreg800.Implementations.System.Generic.GenericCls
	:members:
	:undoc-members:
	:noindex: