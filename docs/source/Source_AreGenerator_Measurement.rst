Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MEASurement:KEEPsettings

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MEASurement:KEEPsettings



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: