Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:RANGe

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:RANGe



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Object.SubChannel.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: