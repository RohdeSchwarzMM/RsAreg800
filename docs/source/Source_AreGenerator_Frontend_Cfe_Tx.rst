Tx<TxIndexNull>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr0 .. Nr15
	rc = driver.source.areGenerator.frontend.cfe.tx.repcap_txIndexNull_get()
	driver.source.areGenerator.frontend.cfe.tx.repcap_txIndexNull_set(repcap.TxIndexNull.Nr0)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Tx.TxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.cfe.tx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Cfe_Tx_Efrontend.rst
	Source_AreGenerator_Frontend_Cfe_Tx_Ota.rst