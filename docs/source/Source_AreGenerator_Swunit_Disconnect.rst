Disconnect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:DISConnect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:DISConnect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.Disconnect.DisconnectCls
	:members:
	:undoc-members:
	:noindex: