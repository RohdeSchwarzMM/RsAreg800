Swunit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:HOSTname
	single: [SOURce<HW>]:AREGenerator:SWUNit:RX
	single: [SOURce<HW>]:AREGenerator:SWUNit:STATus
	single: [SOURce<HW>]:AREGenerator:SWUNit:TX

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:HOSTname
	[SOURce<HW>]:AREGenerator:SWUNit:RX
	[SOURce<HW>]:AREGenerator:SWUNit:STATus
	[SOURce<HW>]:AREGenerator:SWUNit:TX



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.SwunitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.swunit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Swunit_CableCorr.rst
	Source_AreGenerator_Swunit_Connect.rst
	Source_AreGenerator_Swunit_Disconnect.rst
	Source_AreGenerator_Swunit_Mapping.rst
	Source_AreGenerator_Swunit_Relays.rst