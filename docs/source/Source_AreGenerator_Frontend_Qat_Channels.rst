Channels
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CHANnels

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CHANnels



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Channels.ChannelsCls
	:members:
	:undoc-members:
	:noindex: