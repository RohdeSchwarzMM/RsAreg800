Antenna
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Antenna_Custom.rst