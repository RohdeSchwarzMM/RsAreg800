Connect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:CONNect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:CONNect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Connect.ConnectCls
	:members:
	:undoc-members:
	:noindex: