Connect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:CONNect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:CONNect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.Connect.ConnectCls
	:members:
	:undoc-members:
	:noindex: