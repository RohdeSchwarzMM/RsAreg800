File
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CABLecorr:CONNector<DI>:RX<ST>:USER:FILE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CABLecorr:CONNector<DI>:RX<ST>:USER:FILE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.CableCorr.Connector.Rx.User.File.FileCls
	:members:
	:undoc-members:
	:noindex: