User
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.CableCorr.Connector.Tx.User.UserCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.swunit.cableCorr.connector.tx.user.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Swunit_CableCorr_Connector_Tx_User_Attenuation.rst
	Source_AreGenerator_Swunit_CableCorr_Connector_Tx_User_Delay.rst
	Source_AreGenerator_Swunit_CableCorr_Connector_Tx_User_File.rst