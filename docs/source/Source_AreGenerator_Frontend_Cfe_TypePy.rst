TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:TYPE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:TYPE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: