Network
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:SYST:NETWork:MACaddress
	single: SYSTem:COMMunicate:SYST:NETWork:STATus

.. code-block:: python

	SYSTem:COMMunicate:SYST:NETWork:MACaddress
	SYSTem:COMMunicate:SYST:NETWork:STATus



.. autoclass:: RsAreg800.Implementations.System.Communicate.Syst.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.syst.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Syst_Network_Common.rst
	System_Communicate_Syst_Network_IpAddress.rst
	System_Communicate_Syst_Network_Restart.rst