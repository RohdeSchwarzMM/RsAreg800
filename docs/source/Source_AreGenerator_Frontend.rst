Frontend
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.FrontendCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Antenna.rst
	Source_AreGenerator_Frontend_Cfe.rst
	Source_AreGenerator_Frontend_Fe.rst
	Source_AreGenerator_Frontend_Last.rst
	Source_AreGenerator_Frontend_Qat.rst
	Source_AreGenerator_Frontend_Trx.rst