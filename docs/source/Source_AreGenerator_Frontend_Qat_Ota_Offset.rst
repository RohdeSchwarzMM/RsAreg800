Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:OTA:OFFSet

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:OTA:OFFSet



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Ota.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: