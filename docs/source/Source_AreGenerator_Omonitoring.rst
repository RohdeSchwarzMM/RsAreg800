Omonitoring
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:OMONitoring:HOSTname
	single: [SOURce<HW>]:AREGenerator:OMONitoring:PORT
	single: [SOURce<HW>]:AREGenerator:OMONitoring:[STATe]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OMONitoring:HOSTname
	[SOURce<HW>]:AREGenerator:OMONitoring:PORT
	[SOURce<HW>]:AREGenerator:OMONitoring:[STATe]



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Omonitoring.OmonitoringCls
	:members:
	:undoc-members:
	:noindex: