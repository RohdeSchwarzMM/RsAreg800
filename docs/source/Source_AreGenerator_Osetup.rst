Osetup
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce<HW>:AREGenerator:OSETup:CONFig
	single: [SOURce<HW>]:AREGenerator:OSETup:HOSTname
	single: [SOURce<HW>]:AREGenerator:OSETup:IPADdress
	single: [SOURce<HW>]:AREGenerator:OSETup:MODE
	single: [SOURce<HW>]:AREGenerator:OSETup:PORT
	single: [SOURce<HW>]:AREGenerator:OSETup:PROTocol
	single: [SOURce<HW>]:AREGenerator:OSETup:REFerence
	single: [SOURce<HW>]:AREGenerator:OSETup:SOURce
	single: [SOURce<HW>]:AREGenerator:OSETup:TBASe

.. code-block:: python

	SOURce<HW>:AREGenerator:OSETup:CONFig
	[SOURce<HW>]:AREGenerator:OSETup:HOSTname
	[SOURce<HW>]:AREGenerator:OSETup:IPADdress
	[SOURce<HW>]:AREGenerator:OSETup:MODE
	[SOURce<HW>]:AREGenerator:OSETup:PORT
	[SOURce<HW>]:AREGenerator:OSETup:PROTocol
	[SOURce<HW>]:AREGenerator:OSETup:REFerence
	[SOURce<HW>]:AREGenerator:OSETup:SOURce
	[SOURce<HW>]:AREGenerator:OSETup:TBASe



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.OsetupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.osetup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Osetup_Apply.rst
	Source_AreGenerator_Osetup_Bw.rst
	Source_AreGenerator_Osetup_Hil.rst
	Source_AreGenerator_Osetup_MultiInstrument.rst
	Source_AreGenerator_Osetup_Swunit.rst