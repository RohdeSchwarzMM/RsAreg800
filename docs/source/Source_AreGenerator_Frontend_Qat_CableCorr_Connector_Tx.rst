Tx<TxIndexNull>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr0 .. Nr15
	rc = driver.source.areGenerator.frontend.qat.cableCorr.connector.tx.repcap_txIndexNull_get()
	driver.source.areGenerator.frontend.qat.cableCorr.connector.tx.repcap_txIndexNull_set(repcap.TxIndexNull.Nr0)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.CableCorr.Connector.Tx.TxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.qat.cableCorr.connector.tx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Qat_CableCorr_Connector_Tx_Mode.rst
	Source_AreGenerator_Frontend_Qat_CableCorr_Connector_Tx_User.rst