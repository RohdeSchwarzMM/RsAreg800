Subnet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SYST:NETWork:[IPADdress]:SUBNet:MASK

.. code-block:: python

	SYSTem:COMMunicate:SYST:NETWork:[IPADdress]:SUBNet:MASK



.. autoclass:: RsAreg800.Implementations.System.Communicate.Syst.Network.IpAddress.Subnet.SubnetCls
	:members:
	:undoc-members:
	:noindex: