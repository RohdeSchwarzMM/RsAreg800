Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:RX<ST>:USER:DELay

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:CABLecorr:CONNector<DI>:RX<ST>:USER:DELay



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.CableCorr.Connector.Rx.User.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: