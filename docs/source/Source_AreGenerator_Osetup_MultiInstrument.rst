MultiInstrument
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:MODE
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:PRIMary

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:MODE
	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:PRIMary



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.MultiInstrument.MultiInstrumentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.osetup.multiInstrument.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Osetup_MultiInstrument_Connect.rst
	Source_AreGenerator_Osetup_MultiInstrument_Remove.rst
	Source_AreGenerator_Osetup_MultiInstrument_Secondary.rst