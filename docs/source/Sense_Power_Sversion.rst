Sversion
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SVERsion

.. code-block:: python

	SENSe<CH>:[POWer]:SVERsion



.. autoclass:: RsAreg800.Implementations.Sense.Power.Sversion.SversionCls
	:members:
	:undoc-members:
	:noindex: