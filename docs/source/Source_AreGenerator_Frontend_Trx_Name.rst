Name
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:NAME

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:NAME



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Name.NameCls
	:members:
	:undoc-members:
	:noindex: