Cfe<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.areGenerator.frontend.cfe.repcap_channel_get()
	driver.source.areGenerator.frontend.cfe.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.CfeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.cfe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Cfe_Add.rst
	Source_AreGenerator_Frontend_Cfe_Alias.rst
	Source_AreGenerator_Frontend_Cfe_Ats.rst
	Source_AreGenerator_Frontend_Cfe_Rmv.rst
	Source_AreGenerator_Frontend_Cfe_Rts.rst
	Source_AreGenerator_Frontend_Cfe_Rx.rst
	Source_AreGenerator_Frontend_Cfe_Tx.rst
	Source_AreGenerator_Frontend_Cfe_TypePy.rst