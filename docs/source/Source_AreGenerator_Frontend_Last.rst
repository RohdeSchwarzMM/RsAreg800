Last
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:LAST:FE
	single: [SOURce<HW>]:AREGenerator:FRONtend:LAST:QAT

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:LAST:FE
	[SOURce<HW>]:AREGenerator:FRONtend:LAST:QAT



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Last.LastCls
	:members:
	:undoc-members:
	:noindex: