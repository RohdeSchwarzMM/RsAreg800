Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CABLecorr:CONNector<DI>:RX<ST>:MODE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CABLecorr:CONNector<DI>:RX<ST>:MODE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.CableCorr.Connector.Rx.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: