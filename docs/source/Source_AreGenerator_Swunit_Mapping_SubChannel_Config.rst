Config
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:MAPPing<CH>:[SUBChannel<ST>]:CONFig

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:MAPPing<CH>:[SUBChannel<ST>]:CONFig



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.Mapping.SubChannel.Config.ConfigCls
	:members:
	:undoc-members:
	:noindex: