Connector<Connector>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.areGenerator.frontend.fe.cableCorr.connector.repcap_connector_get()
	driver.source.areGenerator.frontend.fe.cableCorr.connector.repcap_connector_set(repcap.Connector.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.CableCorr.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.cableCorr.connector.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Fe_CableCorr_Connector_Rx.rst
	Source_AreGenerator_Frontend_Fe_CableCorr_Connector_Tx.rst