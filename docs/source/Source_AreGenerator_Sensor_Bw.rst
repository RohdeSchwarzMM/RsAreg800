Bw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:BW

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:BW



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Bw.BwCls
	:members:
	:undoc-members:
	:noindex: