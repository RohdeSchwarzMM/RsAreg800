Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CABLecorr:CONNector<DI>:TX<ST0>:USER:DELay

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CABLecorr:CONNector<DI>:TX<ST0>:USER:DELay



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.CableCorr.Connector.Tx.User.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: