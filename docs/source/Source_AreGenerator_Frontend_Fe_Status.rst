Status
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:STATus

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:STATus



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Status.StatusCls
	:members:
	:undoc-members:
	:noindex: