File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:SCENario:FILE:CATalog
	single: [SOURce<HW>]:AREGenerator:SCENario:FILE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SCENario:FILE:CATalog
	[SOURce<HW>]:AREGenerator:SCENario:FILE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Scenario.File.FileCls
	:members:
	:undoc-members:
	:noindex: