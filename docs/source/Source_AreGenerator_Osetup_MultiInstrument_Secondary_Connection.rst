Connection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:CONNection:[STATe]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:CONNection:[STATe]



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex: