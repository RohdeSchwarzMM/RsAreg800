Bw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:BW

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:BW



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Bw.BwCls
	:members:
	:undoc-members:
	:noindex: