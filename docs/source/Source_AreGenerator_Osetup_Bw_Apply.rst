Apply
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:BW:APPLy

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:BW:APPLy



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.Bw.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: