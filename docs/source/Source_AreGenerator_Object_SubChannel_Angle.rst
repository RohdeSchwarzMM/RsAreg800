Angle
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Object.SubChannel.Angle.AngleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.object.subChannel.angle.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Object_SubChannel_Angle_Horizontal.rst