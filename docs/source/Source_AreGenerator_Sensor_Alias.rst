Alias
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:ALIas

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:ALIas



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Alias.AliasCls
	:members:
	:undoc-members:
	:noindex: