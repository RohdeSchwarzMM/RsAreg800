Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CABLecorr:CONNector<DI>:TX<ST0>:USER:DELay

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CABLecorr:CONNector<DI>:TX<ST0>:USER:DELay



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.CableCorr.Connector.Tx.User.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: