Hostname
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:HOSTname

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:HOSTname



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Hostname.HostnameCls
	:members:
	:undoc-members:
	:noindex: