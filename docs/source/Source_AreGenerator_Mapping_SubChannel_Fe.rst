Fe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MAPPing<CH>:[SUBChannel<ST>]:FE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MAPPing<CH>:[SUBChannel<ST>]:FE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.SubChannel.Fe.FeCls
	:members:
	:undoc-members:
	:noindex: