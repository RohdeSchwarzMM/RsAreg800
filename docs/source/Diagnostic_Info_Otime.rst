Otime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:INFO:OTIMe:SET
	single: DIAGnostic:INFO:OTIMe

.. code-block:: python

	DIAGnostic:INFO:OTIMe:SET
	DIAGnostic:INFO:OTIMe



.. autoclass:: RsAreg800.Implementations.Diagnostic.Info.Otime.OtimeCls
	:members:
	:undoc-members:
	:noindex: