Secondary
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:HOSTname
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:HOSTname
	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.SecondaryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.osetup.multiInstrument.secondary.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Add.rst
	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Connection.rst
	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Execute.rst
	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Remove.rst