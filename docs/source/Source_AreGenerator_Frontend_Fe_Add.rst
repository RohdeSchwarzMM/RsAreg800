Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ADD

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ADD



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Add.AddCls
	:members:
	:undoc-members:
	:noindex: