Transaction
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DEXChange:TRANsaction:STATe

.. code-block:: python

	SYSTem:DEXChange:TRANsaction:STATe



.. autoclass:: RsAreg800.Implementations.System.Dexchange.Transaction.TransactionCls
	:members:
	:undoc-members:
	:noindex: