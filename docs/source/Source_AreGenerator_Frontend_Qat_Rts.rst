Rts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:RTS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:RTS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Rts.RtsCls
	:members:
	:undoc-members:
	:noindex: