Points
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic<HW>:EEPRom<CH>:DATA:POINts

.. code-block:: python

	DIAGnostic<HW>:EEPRom<CH>:DATA:POINts



.. autoclass:: RsAreg800.Implementations.Diagnostic.Eeprom.Data.Points.PointsCls
	:members:
	:undoc-members:
	:noindex: