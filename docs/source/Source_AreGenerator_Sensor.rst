Sensor<Sensor>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.areGenerator.sensor.repcap_sensor_get()
	driver.source.areGenerator.sensor.repcap_sensor_set(repcap.Sensor.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Sensor_Add.rst
	Source_AreGenerator_Sensor_Alias.rst
	Source_AreGenerator_Sensor_Angle.rst
	Source_AreGenerator_Sensor_Bw.rst
	Source_AreGenerator_Sensor_Center.rst
	Source_AreGenerator_Sensor_Cfactor.rst
	Source_AreGenerator_Sensor_Count.rst
	Source_AreGenerator_Sensor_Distance.rst
	Source_AreGenerator_Sensor_Dynamic.rst
	Source_AreGenerator_Sensor_Id.rst
	Source_AreGenerator_Sensor_Rmv.rst