Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:RX<ST>:OTA:OFFSet

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:RX<ST>:OTA:OFFSet



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Rx.Ota.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: