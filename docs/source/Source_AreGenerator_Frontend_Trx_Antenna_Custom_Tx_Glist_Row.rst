Row<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.areGenerator.frontend.trx.antenna.custom.tx.glist.row.repcap_index_get()
	driver.source.areGenerator.frontend.trx.antenna.custom.tx.glist.row.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:CUSTom:TX<ST0>:GLISt:ROW<DI>

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:CUSTom:TX<ST0>:GLISt:ROW<DI>



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Antenna.Custom.Tx.Glist.Row.RowCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.trx.antenna.custom.tx.glist.row.clone()