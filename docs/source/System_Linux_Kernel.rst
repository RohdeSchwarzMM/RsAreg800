Kernel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:LINux:KERNel:VERSion

.. code-block:: python

	SYSTem:LINux:KERNel:VERSion



.. autoclass:: RsAreg800.Implementations.System.Linux.Kernel.KernelCls
	:members:
	:undoc-members:
	:noindex: