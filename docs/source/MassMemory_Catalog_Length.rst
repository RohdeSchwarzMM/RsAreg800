Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:CATalog:LENGth

.. code-block:: python

	MMEMory:CATalog:LENGth



.. autoclass:: RsAreg800.Implementations.MassMemory.Catalog.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: