Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:ADD

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:ADD



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Add.AddCls
	:members:
	:undoc-members:
	:noindex: