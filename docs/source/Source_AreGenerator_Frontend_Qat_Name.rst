Name
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:NAME

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:NAME



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Name.NameCls
	:members:
	:undoc-members:
	:noindex: