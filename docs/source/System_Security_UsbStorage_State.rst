State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:USBStorage:[STATe]

.. code-block:: python

	SYSTem:SECurity:USBStorage:[STATe]



.. autoclass:: RsAreg800.Implementations.System.Security.UsbStorage.State.StateCls
	:members:
	:undoc-members:
	:noindex: