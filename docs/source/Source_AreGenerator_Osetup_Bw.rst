Bw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:BW

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:BW



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Osetup.Bw.BwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.osetup.bw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Osetup_Bw_Apply.rst