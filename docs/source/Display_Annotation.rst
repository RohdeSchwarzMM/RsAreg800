Annotation
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Display.Annotation.AnnotationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.annotation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Annotation_All.rst