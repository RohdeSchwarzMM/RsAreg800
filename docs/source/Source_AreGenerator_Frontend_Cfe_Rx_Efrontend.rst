Efrontend
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:RX<ST>:EFRontend

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:RX<ST>:EFRontend



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Rx.Efrontend.EfrontendCls
	:members:
	:undoc-members:
	:noindex: