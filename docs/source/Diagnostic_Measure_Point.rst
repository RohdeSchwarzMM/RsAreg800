Point
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic<HW>:[MEASure]:POINt

.. code-block:: python

	DIAGnostic<HW>:[MEASure]:POINt



.. autoclass:: RsAreg800.Implementations.Diagnostic.Measure.Point.PointCls
	:members:
	:undoc-members:
	:noindex: