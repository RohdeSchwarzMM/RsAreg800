Image
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: HCOPy:IMAGe:FORMat

.. code-block:: python

	HCOPy:IMAGe:FORMat



.. autoclass:: RsAreg800.Implementations.HardCopy.Image.ImageCls
	:members:
	:undoc-members:
	:noindex: