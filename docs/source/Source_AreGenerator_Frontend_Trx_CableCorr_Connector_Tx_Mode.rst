Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CABLecorr:CONNector<DI>:TX<ST0>:MODE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:CABLecorr:CONNector<DI>:TX<ST0>:MODE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.CableCorr.Connector.Tx.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: