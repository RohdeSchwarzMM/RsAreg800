Level
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.Adjust.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.mapping.adjust.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Mapping_Adjust_Level_DigHeadroom.rst
	Source_AreGenerator_Mapping_Adjust_Level_Otime.rst