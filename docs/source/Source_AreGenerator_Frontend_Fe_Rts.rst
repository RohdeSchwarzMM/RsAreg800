Rts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:RTS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:RTS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Rts.RtsCls
	:members:
	:undoc-members:
	:noindex: