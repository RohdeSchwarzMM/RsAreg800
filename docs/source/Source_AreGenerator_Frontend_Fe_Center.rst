Center
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CENTer

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:CENTer



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Center.CenterCls
	:members:
	:undoc-members:
	:noindex: