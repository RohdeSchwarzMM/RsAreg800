System
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:SYSTem:ALIGnment

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:SYSTem:ALIGnment



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Channel.System.SystemCls
	:members:
	:undoc-members:
	:noindex: