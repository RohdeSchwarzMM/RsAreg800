Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MAPPing<CH>:[SUBChannel<ST>]:ADJust:LEVel

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MAPPing<CH>:[SUBChannel<ST>]:ADJust:LEVel



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.SubChannel.Adjust.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: