Center
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:CENTer

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:CENTer



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Center.CenterCls
	:members:
	:undoc-members:
	:noindex: