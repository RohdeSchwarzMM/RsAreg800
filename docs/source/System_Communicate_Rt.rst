Rt
----------------------------------------





.. autoclass:: RsAreg800.Implementations.System.Communicate.Rt.RtCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.rt.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Rt_Network.rst