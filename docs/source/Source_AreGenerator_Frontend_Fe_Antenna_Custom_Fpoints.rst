Fpoints
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:FPOints

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:FPOints



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Antenna.Custom.Fpoints.FpointsCls
	:members:
	:undoc-members:
	:noindex: