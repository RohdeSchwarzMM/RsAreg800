Ats
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ATS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ATS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Ats.AtsCls
	:members:
	:undoc-members:
	:noindex: