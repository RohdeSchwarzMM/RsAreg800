Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:CUSTom:[MODE]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:CUSTom:[MODE]



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Antenna.Custom.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: