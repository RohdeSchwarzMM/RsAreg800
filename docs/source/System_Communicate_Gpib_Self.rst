Self
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:GPIB:[SELF]:ADDRess

.. code-block:: python

	SYSTem:COMMunicate:GPIB:[SELF]:ADDRess



.. autoclass:: RsAreg800.Implementations.System.Communicate.Gpib.Self.SelfCls
	:members:
	:undoc-members:
	:noindex: