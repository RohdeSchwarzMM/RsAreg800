Predefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:ANTenna:CUSTom:IMPort:PREDefined:CATalog

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:ANTenna:CUSTom:IMPort:PREDefined:CATalog



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Antenna.Custom.ImportPy.Predefined.PredefinedCls
	:members:
	:undoc-members:
	:noindex: