Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CABLecorr:CONNector<DI>:RX<ST>:USER:ATTenuation

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:CABLecorr:CONNector<DI>:RX<ST>:USER:ATTenuation



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.CableCorr.Connector.Rx.User.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: