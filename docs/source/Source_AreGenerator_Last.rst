Last
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:LAST:SENSor

.. code-block:: python

	[SOURce<HW>]:AREGenerator:LAST:SENSor



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Last.LastCls
	:members:
	:undoc-members:
	:noindex: