Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:GAIN:RX

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ANTenna:GAIN:RX



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Antenna.Gain.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: