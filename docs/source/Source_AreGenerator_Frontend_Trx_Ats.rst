Ats
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ATS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ATS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Ats.AtsCls
	:members:
	:undoc-members:
	:noindex: