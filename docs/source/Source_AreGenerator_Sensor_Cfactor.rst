Cfactor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:CFACtor

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:CFACtor



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Cfactor.CfactorCls
	:members:
	:undoc-members:
	:noindex: