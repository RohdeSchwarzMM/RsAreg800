Qat<QatFrontent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.areGenerator.frontend.qat.repcap_qatFrontent_get()
	driver.source.areGenerator.frontend.qat.repcap_qatFrontent_set(repcap.QatFrontent.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.QatCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.qat.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Qat_Add.rst
	Source_AreGenerator_Frontend_Qat_Alias.rst
	Source_AreGenerator_Frontend_Qat_Ats.rst
	Source_AreGenerator_Frontend_Qat_Bw.rst
	Source_AreGenerator_Frontend_Qat_CableCorr.rst
	Source_AreGenerator_Frontend_Qat_Center.rst
	Source_AreGenerator_Frontend_Qat_Channels.rst
	Source_AreGenerator_Frontend_Qat_Connect.rst
	Source_AreGenerator_Frontend_Qat_Disconnect.rst
	Source_AreGenerator_Frontend_Qat_Hostname.rst
	Source_AreGenerator_Frontend_Qat_Id.rst
	Source_AreGenerator_Frontend_Qat_IpAddress.rst
	Source_AreGenerator_Frontend_Qat_Mode.rst
	Source_AreGenerator_Frontend_Qat_Name.rst
	Source_AreGenerator_Frontend_Qat_Or.rst
	Source_AreGenerator_Frontend_Qat_Ota.rst
	Source_AreGenerator_Frontend_Qat_Rmv.rst
	Source_AreGenerator_Frontend_Qat_Rts.rst
	Source_AreGenerator_Frontend_Qat_Snumber.rst
	Source_AreGenerator_Frontend_Qat_Status.rst
	Source_AreGenerator_Frontend_Qat_TypePy.rst