Rx<RxIndex>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.source.areGenerator.frontend.trx.cableCorr.connector.rx.repcap_rxIndex_get()
	driver.source.areGenerator.frontend.trx.cableCorr.connector.rx.repcap_rxIndex_set(repcap.RxIndex.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.CableCorr.Connector.Rx.RxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.trx.cableCorr.connector.rx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Trx_CableCorr_Connector_Rx_Mode.rst
	Source_AreGenerator_Frontend_Trx_CableCorr_Connector_Rx_User.rst