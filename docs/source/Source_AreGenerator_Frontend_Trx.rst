Trx<TrxFrontent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.source.areGenerator.frontend.trx.repcap_trxFrontent_get()
	driver.source.areGenerator.frontend.trx.repcap_trxFrontent_set(repcap.TrxFrontent.Nr1)





.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.TrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.trx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Trx_Alias.rst
	Source_AreGenerator_Frontend_Trx_Antenna.rst
	Source_AreGenerator_Frontend_Trx_Ats.rst
	Source_AreGenerator_Frontend_Trx_Bw.rst
	Source_AreGenerator_Frontend_Trx_CableCorr.rst
	Source_AreGenerator_Frontend_Trx_Center.rst
	Source_AreGenerator_Frontend_Trx_Eirp.rst
	Source_AreGenerator_Frontend_Trx_Gain.rst
	Source_AreGenerator_Frontend_Trx_Id.rst
	Source_AreGenerator_Frontend_Trx_Name.rst
	Source_AreGenerator_Frontend_Trx_Ota.rst
	Source_AreGenerator_Frontend_Trx_Rts.rst
	Source_AreGenerator_Frontend_Trx_Snumber.rst
	Source_AreGenerator_Frontend_Trx_TypePy.rst