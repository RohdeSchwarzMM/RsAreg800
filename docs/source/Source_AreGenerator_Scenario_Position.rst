Position
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:SCENario:POSition:ACTual
	single: [SOURce<HW>]:AREGenerator:SCENario:POSition:STARt
	single: [SOURce<HW>]:AREGenerator:SCENario:POSition:STOP

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SCENario:POSition:ACTual
	[SOURce<HW>]:AREGenerator:SCENario:POSition:STARt
	[SOURce<HW>]:AREGenerator:SCENario:POSition:STOP



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Scenario.Position.PositionCls
	:members:
	:undoc-members:
	:noindex: