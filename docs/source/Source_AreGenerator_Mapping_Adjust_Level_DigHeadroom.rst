DigHeadroom
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MAPPing<CH>:ADJust:LEVel:DIGHeadroom

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MAPPing<CH>:ADJust:LEVel:DIGHeadroom



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.Adjust.Level.DigHeadroom.DigHeadroomCls
	:members:
	:undoc-members:
	:noindex: