Network
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:RT:NETWork:MACaddress
	single: SYSTem:COMMunicate:RT:NETWork:STATus

.. code-block:: python

	SYSTem:COMMunicate:RT:NETWork:MACaddress
	SYSTem:COMMunicate:RT:NETWork:STATus



.. autoclass:: RsAreg800.Implementations.System.Communicate.Rt.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.rt.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Rt_Network_Common.rst
	System_Communicate_Rt_Network_IpAddress.rst
	System_Communicate_Rt_Network_Restart.rst