Ats
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ATS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ATS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Ats.AtsCls
	:members:
	:undoc-members:
	:noindex: