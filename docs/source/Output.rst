Output
----------------------------------------





.. autoclass:: RsAreg800.Implementations.Output.OutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.output.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Output_User.rst