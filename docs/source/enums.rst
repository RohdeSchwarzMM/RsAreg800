Enums
=========

AregAttRcsKeepConst
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregAttRcsKeepConst.ATTenuation
	# All values (2x):
	ATTenuation | RCS

AregCableCorrSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregCableCorrSour.FACTory
	# All values (3x):
	FACTory | S2P | USER

AregCconfigBw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregCconfigBw.BW1G
	# All values (3x):
	BW1G | BW2G | BW5G

AregCconfigOptMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregCconfigOptMode.FAST
	# All values (2x):
	FAST | QHIG

AregCconfigSystAlign
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregCconfigSystAlign.OFF
	# All values (3x):
	OFF | ON | TABLe

AregChanMappingGui
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AregChanMappingGui.CFE1
	# Last value:
	value = enums.AregChanMappingGui.TRX4
	# All values (82x):
	CFE1 | CFE2 | CFE3 | CFE4 | CFE5 | CFE6 | CFE7 | CFE8
	FE1 | FE2 | FE3 | FE4 | IFONly | NONE | QAT1CH1 | QAT1CH2
	QAT1CH3 | QAT1CH4 | QAT1CH5 | QAT1CH6 | QAT1CH7 | QAT1CH8 | QAT2CH1 | QAT2CH2
	QAT2CH3 | QAT2CH4 | QAT2CH5 | QAT2CH6 | QAT2CH7 | QAT2CH8 | QAT3CH1 | QAT3CH2
	QAT3CH3 | QAT3CH4 | QAT3CH5 | QAT3CH6 | QAT3CH7 | QAT3CH8 | QAT4CH1 | QAT4CH2
	QAT4CH3 | QAT4CH4 | QAT4CH5 | QAT4CH6 | QAT4CH7 | QAT4CH8 | QAT5CH1 | QAT5CH2
	QAT5CH3 | QAT5CH4 | QAT5CH5 | QAT5CH6 | QAT5CH7 | QAT5CH8 | QAT6CH1 | QAT6CH2
	QAT6CH3 | QAT6CH4 | QAT6CH5 | QAT6CH6 | QAT6CH7 | QAT6CH8 | QAT7CH1 | QAT7CH2
	QAT7CH3 | QAT7CH4 | QAT7CH5 | QAT7CH6 | QAT7CH7 | QAT7CH8 | QAT8CH1 | QAT8CH2
	QAT8CH3 | QAT8CH4 | QAT8CH5 | QAT8CH6 | QAT8CH7 | QAT8CH8 | TRX1 | TRX2
	TRX3 | TRX4

AregChanMappingSensor
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AregChanMappingSensor.NONE
	# Last value:
	value = enums.AregChanMappingSensor.SEN8
	# All values (9x):
	NONE | SEN1 | SEN2 | SEN3 | SEN4 | SEN5 | SEN6 | SEN7
	SEN8

AregDopplerUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregDopplerUnit.FREQuency
	# All values (2x):
	FREQuency | SPEed

AregDynLoggLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregDynLoggLevel.ALL
	# All values (3x):
	ALL | EAWarning | ERRor

AregFconfUseCustAntAreg800
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregFconfUseCustAntAreg800.LIST
	# All values (2x):
	LIST | NONe

AregFeQatConnMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregFeQatConnMode.CERRor
	# All values (6x):
	CERRor | CONNected | DIALing | DISConnected | UERRor | UPDate

AregFeQatMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregFeQatMode.MULTi
	# All values (2x):
	MULTi | SINGle

AregFeQatOrientation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregFeQatOrientation.HORizontal
	# All values (2x):
	HORizontal | VERTical

AregFeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregFeType.CFE
	# All values (5x):
	CFE | FE | NONE | QAT | TRX

AregHilUpdateMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregHilUpdateMode.IMMediate
	# All values (2x):
	IMMediate | TIMestamp

AregMeasPort
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregMeasPort.AUX
	# All values (2x):
	AUX | POW

AregMultiInstCnctStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregMultiInstCnctStatus.CERRor
	# All values (5x):
	CERRor | CONNected | DISConnected | TCONnencting | TDISconnecting

AregMultiInstMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregMultiInstMode.OFF
	# All values (3x):
	OFF | PRIMary | SECondary

AregObjMarkSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregObjMarkSource.HIL
	# All values (3x):
	HIL | SCENario | SETTing

AregPlEd
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregPlEd.ERRor
	# All values (4x):
	ERRor | INACtive | OK | WARNing

AregPowSens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregPowSens.SEN1
	# All values (5x):
	SEN1 | SEN2 | SEN3 | SEN4 | UDEFined

AregRadarPowIndicator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregRadarPowIndicator.BAD
	# All values (4x):
	BAD | GOOD | OFF | WEAK

AregSetupTimeBase
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregSetupTimeBase.SIMulation
	# All values (2x):
	SIMulation | SYSTem

ByteOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ByteOrder.NORMal
	# All values (2x):
	NORMal | SWAPped

CalDataMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataMode.CUSTomer
	# All values (2x):
	CUSTomer | FACTory

CalDataUpdate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataUpdate.BBFRC
	# All values (6x):
	BBFRC | FREQuency | IALL | LEVel | LEVForced | RFFRC

Colour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Colour.GREen
	# All values (4x):
	GREen | NONE | RED | YELLow

ConnDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnDirection.INPut
	# All values (3x):
	INPut | OUTPut | UNUSed

CustAntFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CustAntFormat.CSV
	# All values (2x):
	CSV | TXT

DevExpFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DevExpFormat.CGPRedefined
	# All values (4x):
	CGPRedefined | CGUSer | SCPI | XML

DispKeybLockMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DispKeybLockMode.DISabled
	# All values (5x):
	DISabled | DONLy | ENABled | TOFF | VNConly

ErFpowSensMapping
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ErFpowSensMapping.SENS1
	# Last value:
	value = enums.ErFpowSensMapping.UNMapped
	# All values (9x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4
	UNMapped

ErFpowSensSourceAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ErFpowSensSourceAreg.USER
	# All values (1x):
	USER

FormData
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormData.ASCii
	# All values (2x):
	ASCii | PACKed

FormStatReg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormStatReg.ASCii
	# All values (4x):
	ASCii | BINary | HEXadecimal | OCTal

FrontPanelLayout
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrontPanelLayout.DIGits
	# All values (2x):
	DIGits | LETTers

HardCopyImageFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyImageFormat.BMP
	# All values (4x):
	BMP | JPG | PNG | XPM

HardCopyRegion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyRegion.ALL
	# All values (2x):
	ALL | DIALog

HilDataReceive
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HilDataReceive.NOData
	# All values (3x):
	NOData | NOTHil | RECeived

IecDevId
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecDevId.AUTO
	# All values (2x):
	AUTO | USER

IecTermMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecTermMode.EOI
	# All values (2x):
	EOI | STANdard

ImpG50G1KcoerceG10K
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ImpG50G1KcoerceG10K.G1K
	# All values (2x):
	G1K | G50

InclExcl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InclExcl.EXCLude
	# All values (2x):
	EXCLude | INCLude

KbLayout
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.KbLayout.CHINese
	# Last value:
	value = enums.KbLayout.SWEDish
	# All values (20x):
	CHINese | DANish | DUTBe | DUTCh | ENGLish | ENGUK | ENGUS | FINNish
	FREBe | FRECa | FRENch | GERMan | ITALian | JAPanese | KORean | NORWegian
	PORTuguese | RUSSian | SPANish | SWEDish

NetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetMode.AUTO
	# All values (2x):
	AUTO | STATic

OsetupBw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OsetupBw.BW1G
	# All values (4x):
	BW1G | BW2G | BW5G | SERVice

OsetupConfiguration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OsetupConfiguration.NR
	# All values (2x):
	NR | STD

OsetupDataSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OsetupDataSource.HIL
	# All values (2x):
	HIL | SCENario

OsetupHilProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OsetupHilProtocol.DCP
	# All values (4x):
	DCP | UDP | UDPR | ZMQ

OsetupMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OsetupMode.DYNamic
	# All values (2x):
	DYNamic | STATic

OsetupObjRef
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OsetupObjRef.MAPPed
	# All values (2x):
	MAPPed | ORIGin

OutpConnGlbSignalAreg800A
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OutpConnGlbSignalAreg800A.OBJect
	# All values (2x):
	OBJect | SWUNit

Parity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Parity.EVEN
	# All values (3x):
	EVEN | NONE | ODD

PixelTestPredefined
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PixelTestPredefined.AUTO
	# Last value:
	value = enums.PixelTestPredefined.WHITe
	# All values (9x):
	AUTO | BLACk | BLUE | GR25 | GR50 | GR75 | GREen | RED
	WHITe

PowSensDisplayPriority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensDisplayPriority.AVERage
	# All values (2x):
	AVERage | PEAK

PowSensFiltType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensFiltType.AUTO
	# All values (3x):
	AUTO | NSRatio | USER

RecScpiCmdMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RecScpiCmdMode.AUTO
	# All values (4x):
	AUTO | DAUTo | MANual | OFF

RoscBandWidtExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscBandWidtExt.NARRow
	# All values (2x):
	NARRow | WIDE

RoscFreqExtAreg800A
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscFreqExtAreg800A._10MHZ
	# All values (2x):
	_10MHZ | _3200MHZ

RoscOutpFreqModeSmbb
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscOutpFreqModeSmbb.DER10M
	# All values (3x):
	DER10M | LOOPthrough | OFF

RoscSourSetup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscSourSetup.ELOop
	# All values (3x):
	ELOop | EXTernal | INTernal

Rs232BdRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232BdRate._115200
	# All values (7x):
	_115200 | _19200 | _2400 | _38400 | _4800 | _57600 | _9600

Rs232StopBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232StopBits._1
	# All values (2x):
	_1 | _2

ScenarioReplyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ScenarioReplyMode.LOOP
	# All values (2x):
	LOOP | SINGle

ScenarioStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ScenarioStatus.RUNNing
	# All values (2x):
	RUNNing | STOPped

SelftLev
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLev.CUSTomer
	# All values (3x):
	CUSTomer | PRODuction | SERVice

SelftLevWrite
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLevWrite.CUSTomer
	# All values (4x):
	CUSTomer | NONE | PRODuction | SERVice

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

StateExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StateExtended.DEFault
	# All values (3x):
	DEFault | OFF | ON

Test
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Test._0
	# All values (4x):
	_0 | _1 | RUNning | STOPped

TestCalSelected
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestCalSelected._0
	# All values (2x):
	_0 | _1

TimeProtocolWithGptp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeProtocolWithGptp._0
	# All values (7x):
	_0 | _1 | GPTP | NONE | NTP | OFF | ON

UnitAngle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitAngle.DEGree
	# All values (3x):
	DEGree | DEGRee | RADian

UnitAngleAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitAngleAreg.DEGree
	# All values (2x):
	DEGree | RADian

UnitLengthAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitLengthAreg.CM
	# All values (3x):
	CM | FT | M

UnitPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPower.DBM
	# All values (3x):
	DBM | DBUV | V

UnitPowSens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPowSens.DBM
	# All values (3x):
	DBM | DBUV | WATT

UnitRcsAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitRcsAreg.DBSM
	# All values (2x):
	DBSM | SM

UnitShiftAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitShiftAreg.HZ
	# All values (3x):
	HZ | KHZ | MHZ

UnitSpeed
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitSpeed.KMH
	# All values (4x):
	KMH | MPH | MPS | NMPH

UnitSpeedAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitSpeedAreg.KMH
	# All values (3x):
	KMH | MPH | MPS

UpdPolicyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpdPolicyMode.CONFirm
	# All values (3x):
	CONFirm | IGNore | STRict

