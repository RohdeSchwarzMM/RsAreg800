Relays
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SWUNit:RELays:CATalog

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SWUNit:RELays:CATalog



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Swunit.Relays.RelaysCls
	:members:
	:undoc-members:
	:noindex: