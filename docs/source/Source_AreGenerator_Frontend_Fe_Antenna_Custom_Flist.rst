Flist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:FLISt

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:FLISt



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Antenna.Custom.Flist.FlistCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.antenna.custom.flist.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Flist_Row.rst