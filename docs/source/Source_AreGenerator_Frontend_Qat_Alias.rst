Alias
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ALIas

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ALIas



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Alias.AliasCls
	:members:
	:undoc-members:
	:noindex: