Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: READ<CH>:[POWer]

.. code-block:: python

	READ<CH>:[POWer]



.. autoclass:: RsAreg800.Implementations.Read.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: