Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:ID

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:ID



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Id.IdCls
	:members:
	:undoc-members:
	:noindex: