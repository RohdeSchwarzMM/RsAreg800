Disconnect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:DISConnect

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:DISConnect



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Disconnect.DisconnectCls
	:members:
	:undoc-members:
	:noindex: