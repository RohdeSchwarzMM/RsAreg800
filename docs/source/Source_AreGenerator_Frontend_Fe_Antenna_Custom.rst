Custom
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:EXPort

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:ANTenna:CUSTom:EXPort



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Antenna.Custom.CustomCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.frontend.fe.antenna.custom.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Flist.rst
	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Fpoints.rst
	Source_AreGenerator_Frontend_Fe_Antenna_Custom_ImportPy.rst
	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Rx.rst
	Source_AreGenerator_Frontend_Fe_Antenna_Custom_Tx.rst