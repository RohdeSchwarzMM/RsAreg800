Sensor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:MAPPing<CH>:SENSor

.. code-block:: python

	[SOURce<HW>]:AREGenerator:MAPPing<CH>:SENSor



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Mapping.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex: