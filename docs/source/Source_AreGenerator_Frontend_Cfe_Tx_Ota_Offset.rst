Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:TX<ST0>:OTA:OFFSet

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:CFE<CH>:TX<ST0>:OTA:OFFSet



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Cfe.Tx.Ota.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: