Gain
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:GAIN

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:GAIN



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: