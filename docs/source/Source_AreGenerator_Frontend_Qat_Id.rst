Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ID

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:ID



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Id.IdCls
	:members:
	:undoc-members:
	:noindex: