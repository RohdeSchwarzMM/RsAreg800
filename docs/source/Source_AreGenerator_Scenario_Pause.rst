Pause
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SCENario:PAUSe

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SCENario:PAUSe



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Scenario.Pause.PauseCls
	:members:
	:undoc-members:
	:noindex: