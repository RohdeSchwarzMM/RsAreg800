Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ID

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:TRX<CH>:ID



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Trx.Id.IdCls
	:members:
	:undoc-members:
	:noindex: