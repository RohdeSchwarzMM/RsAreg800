Internal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:DEVice:INTernal

.. code-block:: python

	TEST:DEVice:INTernal



.. autoclass:: RsAreg800.Implementations.Test.Device.Internal.InternalCls
	:members:
	:undoc-members:
	:noindex: