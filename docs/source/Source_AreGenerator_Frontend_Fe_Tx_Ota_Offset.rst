Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:TX<ST0>:OTA:OFFSet

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:FE<CH>:TX<ST0>:OTA:OFFSet



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Fe.Tx.Ota.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: