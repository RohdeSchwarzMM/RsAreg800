Distance
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:SENSor<CH>:DISTance

.. code-block:: python

	[SOURce<HW>]:AREGenerator:SENSor<CH>:DISTance



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Sensor.Distance.DistanceCls
	:members:
	:undoc-members:
	:noindex: