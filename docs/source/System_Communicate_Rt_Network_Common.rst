Common
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:RT:NETWork:[COMMon]:HOSTname
	single: SYSTem:COMMunicate:RT:NETWork:[COMMon]:WORKgroup

.. code-block:: python

	SYSTem:COMMunicate:RT:NETWork:[COMMon]:HOSTname
	SYSTem:COMMunicate:RT:NETWork:[COMMon]:WORKgroup



.. autoclass:: RsAreg800.Implementations.System.Communicate.Rt.Network.Common.CommonCls
	:members:
	:undoc-members:
	:noindex: