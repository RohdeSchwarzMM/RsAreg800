Path
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:PATH:COUNt

.. code-block:: python

	[SOURce]:PATH:COUNt



.. autoclass:: RsAreg800.Implementations.Source.Path.PathCls
	:members:
	:undoc-members:
	:noindex: