Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:MODE

.. code-block:: python

	[SOURce<HW>]:AREGenerator:FRONtend:QAT<CH>:MODE



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Frontend.Qat.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: