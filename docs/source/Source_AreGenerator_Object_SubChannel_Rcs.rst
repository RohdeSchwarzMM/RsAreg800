Rcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:RCS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:RCS



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Object.SubChannel.Rcs.RcsCls
	:members:
	:undoc-members:
	:noindex: