Channel
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:BW
	single: [SOURce<HW>]:AREGenerator:CHANnel:CATalog
	single: [SOURce<HW>]:AREGenerator:CHANnel:ID
	single: [SOURce<HW>]:AREGenerator:CHANnel:NAME
	single: [SOURce<HW>]:AREGenerator:CHANnel:[STATe]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:BW
	[SOURce<HW>]:AREGenerator:CHANnel:CATalog
	[SOURce<HW>]:AREGenerator:CHANnel:ID
	[SOURce<HW>]:AREGenerator:CHANnel:NAME
	[SOURce<HW>]:AREGenerator:CHANnel:[STATe]



.. autoclass:: RsAreg800.Implementations.Source.AreGenerator.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Channel_Condition.rst
	Source_AreGenerator_Channel_InputPy.rst
	Source_AreGenerator_Channel_Level.rst
	Source_AreGenerator_Channel_Optimization.rst
	Source_AreGenerator_Channel_Output.rst
	Source_AreGenerator_Channel_System.rst