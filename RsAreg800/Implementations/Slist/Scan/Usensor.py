from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal.Types import DataType
from ....Internal.ArgSingleList import ArgSingleList
from ....Internal.ArgSingle import ArgSingle


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class UsensorCls:
	"""Usensor commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("usensor", core, parent)

	def set(self, device_id: str, serial: int) -> None:
		"""SCPI: SLISt:SCAN:USENsor \n
		Snippet: driver.slist.scan.usensor.set(device_id = 'abc', serial = 1) \n
		Scans for R&S NRP power sensors connected over a USB interface. \n
			:param device_id: No help available
			:param serial: No help available
		"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('device_id', device_id, DataType.String), ArgSingle('serial', serial, DataType.Integer))
		self._core.io.write(f'SLISt:SCAN:USENsor {param}'.rstrip())
