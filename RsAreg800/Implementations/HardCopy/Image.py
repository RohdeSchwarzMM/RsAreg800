from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ImageCls:
	"""Image commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("image", core, parent)

	# noinspection PyTypeChecker
	def get_format_py(self) -> enums.HardCopyImageFormat:
		"""SCPI: HCOPy:IMAGe:FORMat \n
		Snippet: value: enums.HardCopyImageFormat = driver.hardCopy.image.get_format_py() \n
		Selects the graphic format for the hard copy. You can use both commands alternatively. \n
			:return: format_py: No help available
		"""
		response = self._core.io.query_str('HCOPy:IMAGe:FORMat?')
		return Conversions.str_to_scalar_enum(response, enums.HardCopyImageFormat)

	def set_format_py(self, format_py: enums.HardCopyImageFormat) -> None:
		"""SCPI: HCOPy:IMAGe:FORMat \n
		Snippet: driver.hardCopy.image.set_format_py(format_py = enums.HardCopyImageFormat.BMP) \n
		Selects the graphic format for the hard copy. You can use both commands alternatively. \n
			:param format_py: No help available
		"""
		param = Conversions.enum_scalar_to_str(format_py, enums.HardCopyImageFormat)
		self._core.io.write(f'HCOPy:IMAGe:FORMat {param}')
