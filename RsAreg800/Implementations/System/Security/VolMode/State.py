from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.Types import DataType
from .....Internal.ArgSingleList import ArgSingleList
from .....Internal.ArgSingle import ArgSingle


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class StateCls:
	"""State commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("state", core, parent)

	def set(self, sec_pass_word: str, mmem_prot_state: bool) -> None:
		"""SCPI: SYSTem:SECurity:VOLMode:[STATe] \n
		Snippet: driver.system.security.volMode.state.set(sec_pass_word = 'abc', mmem_prot_state = False) \n
		Activates volatile mode, so that no user data can be written to the internal memory permanently. To enable volatile mode,
		reboot the instrument. Otherwise the change has no effect. \n
			:param sec_pass_word: Current security password The default password is 123456.
			:param mmem_prot_state: No help available
		"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('sec_pass_word', sec_pass_word, DataType.String), ArgSingle('mmem_prot_state', mmem_prot_state, DataType.Boolean))
		self._core.io.write(f'SYSTem:SECurity:VOLMode:STATe {param}'.rstrip())

	def get(self) -> bool:
		"""SCPI: SYSTem:SECurity:VOLMode:[STATe] \n
		Snippet: value: bool = driver.system.security.volMode.state.get() \n
		Activates volatile mode, so that no user data can be written to the internal memory permanently. To enable volatile mode,
		reboot the instrument. Otherwise the change has no effect. \n
			:return: mmem_prot_state: No help available"""
		response = self._core.io.query_str(f'SYSTem:SECurity:VOLMode:STATe?')
		return Conversions.str_to_bool(response)
