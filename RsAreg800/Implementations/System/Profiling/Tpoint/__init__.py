from typing import List

from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TpointCls:
	"""Tpoint commands group definition. 2 total commands, 1 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("tpoint", core, parent)

	@property
	def catalog(self):
		"""catalog commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_catalog'):
			from .Catalog import CatalogCls
			self._catalog = CatalogCls(self._core, self._cmd_group)
		return self._catalog

	def get_restart(self) -> List[str]:
		"""SCPI: SYSTem:PROFiling:TPOint:RESTart \n
		Snippet: value: List[str] = driver.system.profiling.tpoint.get_restart() \n
		No command help available \n
			:return: module_and_tp: No help available
		"""
		response = self._core.io.query_str('SYSTem:PROFiling:TPOint:RESTart?')
		return Conversions.str_to_str_list(response)

	def set_restart(self, module_and_tp: List[str]) -> None:
		"""SCPI: SYSTem:PROFiling:TPOint:RESTart \n
		Snippet: driver.system.profiling.tpoint.set_restart(module_and_tp = ['abc1', 'abc2', 'abc3']) \n
		No command help available \n
			:param module_and_tp: No help available
		"""
		param = Conversions.list_to_csv_quoted_str(module_and_tp)
		self._core.io.write(f'SYSTem:PROFiling:TPOint:RESTart {param}')

	def clone(self) -> 'TpointCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = TpointCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
