from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class AttenuatorCls:
	"""Attenuator commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("attenuator", core, parent)

	def get_stage(self) -> int:
		"""SCPI: CALibration<HW>:LEVel:ATTenuator:STAGe \n
		Snippet: value: int = driver.calibration.level.attenuator.get_stage() \n
		No command help available \n
			:return: stage: No help available
		"""
		response = self._core.io.query_str('CALibration<HwInstance>:LEVel:ATTenuator:STAGe?')
		return Conversions.str_to_int(response)

	def set_stage(self, stage: int) -> None:
		"""SCPI: CALibration<HW>:LEVel:ATTenuator:STAGe \n
		Snippet: driver.calibration.level.attenuator.set_stage(stage = 1) \n
		No command help available \n
			:param stage: No help available
		"""
		param = Conversions.decimal_value_to_str(stage)
		self._core.io.write(f'CALibration<HwInstance>:LEVel:ATTenuator:STAGe {param}')
